import * as _ from 'lodash'

export type ProfileValue = 'nayeli' | 'brenda';

export interface IProfile {
  id: ProfileValue;
  cards: IProfileCard[];
}

export enum Bank {
  HSBC = 'hsbc',
  BBVA = 'bbva',
  Nu = 'nu',
  Rappi = 'rappi',
  HeyBanco = 'hey_banco',
  MercadoPago = 'mercado_pago',
  Klar = 'klar'
}

export interface IProfileCard {
  id: string;
  cutOffDate: number;
  paymentDueDate: number;
  alias: string;
  bank: Bank;
  numbers: IProfileCardNumber[];
  network: 'visa' | 'mastercard'
  show?: boolean
}

export interface IProfileCardNumber {
  endingDigits: string;
  type: 'virtual' | 'physical';
}

export const PROFILES: IProfile[] = [
  {
    id: 'nayeli',
    cards: [
      {
        id: '6176b5e61854abf6726a938b',
        cutOffDate: 13,
        paymentDueDate: 2,
        alias: 'TDC HSBC',
        bank: Bank.HSBC,
        network: 'visa',
        numbers: [
          { endingDigits: '8836', type: 'physical' },
          { endingDigits: '5887', type: 'virtual' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938c',
        cutOffDate: 5,
        paymentDueDate: 25,
        alias: 'TDC BBVA',
        bank: Bank.BBVA,
        network: 'visa',
        numbers: [
          { endingDigits: '0799', type: 'physical' },
          { endingDigits: '0102', type: 'virtual' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938d',
        cutOffDate: 25,
        paymentDueDate: 15,
        alias: 'TDC Hey Banco',
        bank: Bank.HeyBanco,
        network: 'visa',
        numbers: [
          { endingDigits: '1413', type: 'physical' },
          { endingDigits: '4294', type: 'virtual' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938e',
        cutOffDate: 6,
        paymentDueDate: 26,
        alias: 'TDC Rappi',
        bank: Bank.Rappi,
        network: 'visa',
        numbers: [
          { endingDigits: '0564', type: 'virtual' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938f',
        cutOffDate: 18,
        paymentDueDate: 28,
        alias: 'TDC Nu',
        bank: Bank.Nu,
        network: 'mastercard',
        numbers: [
          { endingDigits: '4934', type: 'virtual' },
          { endingDigits: '0820', type: 'physical' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938g',
        cutOffDate: 7,
        paymentDueDate: 17,
        alias: 'TDC Mercado Pago',
        bank: Bank.MercadoPago,
        network: 'visa',
        numbers: [
          { endingDigits: '5188', type: 'virtual' },
        ],
      },
      {
        id: '6176b5e61854abf6726a938h',
        cutOffDate: 29,
        paymentDueDate: 8,
        alias: 'TDC Klar',
        bank: Bank.Klar,
        network: 'mastercard',
        numbers: [
          { endingDigits: '5132', type: 'virtual' },
        ],
      }
    ],
  },
];
