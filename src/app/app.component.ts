import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute, Router,
} from '@angular/router';
import { ProfileBehaviorService } from './shared/services/profile/profile-behavior.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private profileBehaviorService: ProfileBehaviorService
  ) {}

  ngOnInit(): void {
    this.loadProfile();
  }

  private loadProfile() {
    this.route.queryParams.subscribe((params) => {
      if (params['profile']) {
        this.profileBehaviorService.selectProfile(params['profile']);
      }
    });
  }

  public goTo(pagePath: string) {
    const {currentProfile} = this.profileBehaviorService;
    //this.router.navigate([`/${pagePath}?profile=${currentProfile?.id}`])
    this.router.navigateByUrl(`/${pagePath}?profile=${currentProfile?.id}`);

  }
}
