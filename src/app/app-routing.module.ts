import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentCardPageComponent } from './shared/pages/current-card-page/current-card-page.component';
import { ProfileGuard } from './shared/guards/profile/profile.guard';
import { CardsListPageComponent } from './shared/pages/cards-list-page/cards-list-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'current-card', pathMatch: 'full' },
  {
    path: 'current-card',
    component: CurrentCardPageComponent,
    canActivate: [ProfileGuard],
  },
  {
    path: 'cards',
    component: CardsListPageComponent,
    canActivate: [ProfileGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
