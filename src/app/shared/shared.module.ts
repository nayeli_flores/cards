import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentCardPageComponent } from './pages/current-card-page/current-card-page.component';
import { PaymentCardComponent } from './components/payment-card/payment-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';

import { PaymentCardInfoComponent } from './components/payment-card-info/payment-card-info.component';
import { CardsListPageComponent } from './pages/cards-list-page/cards-list-page.component';
import { BankNamePipe } from './pipes/bank-name/bank-name.pipe';

@NgModule({
  declarations: [
    CurrentCardPageComponent,
    PaymentCardComponent,
    PaymentCardInfoComponent,
    CardsListPageComponent,
    BankNamePipe,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
  ],
})
export class SharedModule {}
