import { Component, Input } from '@angular/core';
import { IProfileCard } from 'src/app/common/data/profiles.constant';
import { ProfileBehaviorService } from '../../services/profile/profile-behavior.service';

@Component({
  selector: 'app-payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.sass'],
})
export class PaymentCardComponent {
  @Input() card: IProfileCard | undefined;

  constructor(public profileBehaviorService: ProfileBehaviorService) {

  }

  public get cardNumber(): string {
    const physicalNumber = this.card?.numbers.find(
      ({ type }) => type === 'physical'
    );

    const virtualNumber = this.card?.numbers.find(
      ({ type }) => type === 'virtual'
    );
    
    return physicalNumber?.endingDigits || virtualNumber?.endingDigits || '****';
  }


}
