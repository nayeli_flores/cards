import { Component, Input } from '@angular/core';
import {  IProfileCard } from 'src/app/common/data/profiles.constant';

@Component({
  selector: 'app-payment-card-info',
  templateUrl: './payment-card-info.component.html',
  styleUrls: ['./payment-card-info.component.sass']
})
export class PaymentCardInfoComponent {
  @Input() card: IProfileCard | undefined;

}
