import { Pipe, PipeTransform } from '@angular/core';
import { Bank } from 'src/app/common/data/profiles.constant';

@Pipe({
  name: 'bankName'
})
export class BankNamePipe implements PipeTransform {

  transform(bankName: Bank | undefined): string {
    if(!bankName) {
      return 'Not provided';
    }

    const names: Record<Bank, string> = {
      [Bank.BBVA]: 'BBVA',
      [Bank.HSBC]: 'HSBC',
      [Bank.Nu]: 'Nu',
      [Bank.Rappi]: 'Rappi',
      [Bank.HeyBanco]: 'Hey Banco',
      [Bank.MercadoPago]: 'Mercado Pago',
      [Bank.Klar]: 'Klar'
    }

    return names[bankName];
  }

}
