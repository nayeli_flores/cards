import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IProfile, PROFILES, ProfileValue } from 'src/app/common/data/profiles.constant';

@Injectable({
  providedIn: 'root'
})
export class ProfileBehaviorService {
  private currentProfile$ = new BehaviorSubject<IProfile | null>(null);
  private profiles$ = new BehaviorSubject<IProfile[]>(PROFILES);

  constructor() { }

  public get currentProfile(): IProfile | null {
    return this.currentProfile$.getValue();
  }

  private set currentProfile(profile: IProfile) {
    this.currentProfile$.next(profile);
  }

  public get currentProfileObservable(): Observable<IProfile | null> {
    return this.currentProfile$.asObservable();
  }

  public get profiles(): IProfile[] {
    return this.profiles$.getValue();
  }

  private set profiles(profiles: IProfile[]) {
    this.profiles$.next(profiles);
  }

  public get profilesObservable(): Observable<IProfile[]> {
    return this.profiles$.asObservable();
  }

  public selectProfile(profileValue: ProfileValue) {
    const profile = this.profiles.find(({id}) => id === profileValue);
    if(profile) {
      this.currentProfile = profile;
    }
  }
}
