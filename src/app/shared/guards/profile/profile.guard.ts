import { ActivatedRoute, ActivatedRouteSnapshot,  CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ProfileBehaviorService } from '../../services/profile/profile-behavior.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard {

  constructor(private router: Router, private route: ActivatedRoute) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    const queryParams = route.queryParams;

    if (!queryParams['profile']) {
      const updatedParams = {
        ...queryParams,
        profile: 'nayeli'
      };

      return this.router.createUrlTree([], {
        relativeTo: this.route,
        queryParams: updatedParams,
        queryParamsHandling: 'merge'
      });
    }

    return true;
  }
}
