import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentCardPageComponent } from './current-card-page.component';

describe('CurrentCardPageComponent', () => {
  let component: CurrentCardPageComponent;
  let fixture: ComponentFixture<CurrentCardPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CurrentCardPageComponent]
    });
    fixture = TestBed.createComponent(CurrentCardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
