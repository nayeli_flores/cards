import { Component, OnInit } from '@angular/core';
import { ProfileBehaviorService } from '../../services/profile/profile-behavior.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { IProfileCard } from 'src/app/common/data/profiles.constant';

@Component({
  selector: 'app-current-card-page',
  templateUrl: './current-card-page.component.html',
  styleUrls: ['./current-card-page.component.sass'],
})
export class CurrentCardPageComponent implements OnInit {
  public currentCard: IProfileCard | undefined;

  constructor(public profileBehaviorService: ProfileBehaviorService) {}

  ngOnInit(): void {
    this.calculateCurrentCard();
  }

  public get day(): number {
    const today = moment();
    return today.date();
  }

  public get month(): number {
    const today = moment();
    return today.month() + 1;
  }

  public get year(): number {
    const today = moment();
    return today.year();
  }

  public get fullDate(): string {
    return moment().toISOString();
  }

  private calculateCurrentCard() {
    if (this.profileBehaviorService?.currentProfile?.cards) {
      const { cards } = this.profileBehaviorService.currentProfile;

      const cardsSorted = _.sortBy(cards, 'cutOffDate');

      let nearestCards: IProfileCard[] = [];

      for (const card of cardsSorted) {
        if (this.day <= card.cutOffDate) {
          nearestCards.push(card);
        }
      }

      let cardsWithCalculatedDays: { id: string; remainingDays: number }[] = [];

      for (const card of nearestCards) {
        cardsWithCalculatedDays.push({
          id: card.id,
          remainingDays: card.cutOffDate - this.day,
        });
      }

      let cardWithMinorRemainingDays = _.minBy(
        cardsWithCalculatedDays,
        'remainingDays'
      );

      this.currentCard = cards.find(
        ({ id }) => id === cardWithMinorRemainingDays?.id
      );

      if (!this.currentCard) {
        const thisDate = moment();
        nearestCards = [];

        for (const card of cardsSorted) {
          const cardFullDateNextMonth = moment(
            `${this.year}-${this.month + 1}-${card.cutOffDate}`
          );

          if (thisDate.isBefore(cardFullDateNextMonth)) {
            nearestCards.push(card);
          }
        }

        cardsWithCalculatedDays = [];

        for (const card of nearestCards) {
          const cardFullDateNextMonth = moment(
            `${this.year}-${this.month + 1}-${card.cutOffDate}`
          );

          cardsWithCalculatedDays.push({
            id: card.id,
            remainingDays: cardFullDateNextMonth.diff(thisDate, 'days'),
          });
        }

        cardWithMinorRemainingDays = _.minBy(
          cardsWithCalculatedDays,
          'remainingDays'
        );

        this.currentCard = cards.find(
          ({ id }) => id === cardWithMinorRemainingDays?.id
        );
      }

    }
  }
}
