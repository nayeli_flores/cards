import { Component, OnInit } from '@angular/core';
import { ProfileBehaviorService } from '../../services/profile/profile-behavior.service';
import { IProfileCard } from 'src/app/common/data/profiles.constant';

@Component({
  selector: 'app-cards-list-page',
  templateUrl: './cards-list-page.component.html',
  styleUrls: ['./cards-list-page.component.sass'],
})
export class CardsListPageComponent implements OnInit {
  public cards: IProfileCard[] = [];

  constructor(public profileBehaviorService: ProfileBehaviorService) {}

  ngOnInit(): void {}

  private addShowProperty() {
    const { cards = [] } = this.profileBehaviorService.currentProfile || {
      cards: [],
    };

    cards.forEach((card) => card.show = false);
  }
}
